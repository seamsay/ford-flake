{
  description = "A documentation generator for Fortran.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages.default = let
        version = "6.1.17";

        markdown-include = let
          pname = "markdown-include";
          version = "0.7.2";
        in
          pkgs.python3Packages.buildPythonPackage {
            inherit pname version;
            format = "setuptools";

            src = pkgs.fetchFromGitHub {
              owner = "cmacmackin";
              repo = pname;
              rev = "v${version}";
              hash = "sha256-2pC0K/Z5l7q6sx4FSM4Pi1/5bt1wLZsqOmcbnE47rVs=";
            };

            propagatedBuildInputs = [pkgs.python3Packages.markdown];
          };

        pygments = let
          version = "2.12.0";
        in
          pkgs.python3Packages.buildPythonPackage {
            pname = "pygments";
            inherit version;

            src = pkgs.python3Packages.fetchPypi {
              pname = "Pygments";
              inherit version;
              hash = "sha256-XrEWEY+WEv8e6JrJZDe7a0no8E2KE7UUuib2ICCOJus=";
            };

            doCheck = false;
          };
      in
        pkgs.python3Packages.buildPythonApplication {
          pname = "ford";
          inherit version;

          src = pkgs.python3Packages.fetchPypi {
            pname = "FORD";
            inherit version;
            hash = "sha256-EPjMGVF+prgwglHlkoank8m8RwmMM9o1KKyhaggUOQg=";
          };

          propagatedBuildInputs =
            [pkgs.python3Packages.graphviz pygments markdown-include]
            ++ (with pkgs.python3Packages; [
              beautifulsoup4
              jinja2
              markdown
              python-markdown-math
              toposort
              tqdm
            ]);

          # TODO: Run the tests, currently `python setup.py test` can't run them.
          doCheck = false;

          meta = {
            homepage = "https://forddocs.readthedocs.io/";
            description = "A documentation generator for Fortran.";
            license = pkgs.lib.licenses.gpl3;
          };
        };

      formatter = pkgs.alejandra;
    });
}
